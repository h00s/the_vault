package me.husak.thevault;

import android.os.Bundle;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.AdapterView.OnItemClickListener;

public class EntriesActivity extends ListActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_entries);

		ListView listEntries = getListView();
		
		EntriesOpenHelper entriesHelper = new EntriesOpenHelper(listEntries.getContext());
		setListAdapter(getEntriesAdapter(entriesHelper));
		entriesHelper.close();
		
		listEntries.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Cursor cursor = (Cursor) getListView().getItemAtPosition(position);
				Intent intent = new Intent(EntriesActivity.this, EntryActivity.class);
				intent.putExtra("id", cursor.getInt(0));
				startActivity(intent);
			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.entries, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case R.id.add_entry:
				Intent intent = new Intent(EntriesActivity.this, EntryActivity.class);
				intent.putExtra("id", -1);
				startActivity(intent);	        	
	        	return true;
	        case R.id.refresh_entries:
	    		EntriesOpenHelper entriesHelper = new EntriesOpenHelper(getBaseContext());
	    		setListAdapter(getEntriesAdapter(entriesHelper));
	    		entriesHelper.close();
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
	
	private ListAdapter getEntriesAdapter(EntriesOpenHelper entriesHelper) {
		Cursor cursor = entriesHelper.getEntries();
		ListAdapter adapter = new SimpleCursorAdapter(this,
				R.layout.entries_item,
				cursor,
				new String[] { EntriesOpenHelper.KEY_TITLE, EntriesOpenHelper.KEY_USERNAME },
				new int[] { R.id.entry_item_name, R.id.entry_item_username },
				0);
		return adapter;
	}
}
