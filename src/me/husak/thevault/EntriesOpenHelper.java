package me.husak.thevault;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class EntriesOpenHelper extends SQLiteOpenHelper {
	
	private static final String DATABASE_NAME = "the_vault.db";
	private static final int DATABASE_VERSION = 1;
	
	private static final String ENTRIES_TABLE_NAME = "entries";
	
	public static final String KEY_ID = "_id";
	public static final String KEY_TITLE = "title";
	public static final String KEY_USERNAME = "username";
	public static final String KEY_PASSWORD = "password";
	
	
    private static final String ENTRIES_TABLE_CREATE =
                "CREATE TABLE " + ENTRIES_TABLE_NAME + " (" +
                KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                KEY_TITLE + " TEXT, " +
                KEY_USERNAME + " TEXT, " +
                KEY_PASSWORD + " TEXT);";

	EntriesOpenHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(ENTRIES_TABLE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}
	
	public Cursor getEntries() {
		SQLiteDatabase db = this.getReadableDatabase();
		return db.query(ENTRIES_TABLE_NAME, null, null, null, null, null, null);
	}
	
	public Entry getEntry(int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.query(ENTRIES_TABLE_NAME, new String[] { KEY_TITLE, KEY_USERNAME, KEY_PASSWORD }, "_id = " + Integer.toString(id), null, null, null, null);
		cursor.moveToFirst();
		Entry entry = new Entry(id, cursor.getString(0), cursor.getString(1), cursor.getString(2));
		cursor.close();
		return entry;
	}
	
	public void saveEntry(Entry entry) {
		SQLiteDatabase db = this.getReadableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_TITLE, entry.getTitle()); 
		values.put(KEY_USERNAME, entry.getUsername());
		values.put(KEY_PASSWORD, entry.getPassword());
		
		if (entry.getId() == -1) {
			db.insert(ENTRIES_TABLE_NAME, null, values);
		} 
		else {
			db.update(ENTRIES_TABLE_NAME, values, "_id=?", new String[] { Integer.toString(entry.getId()) });
		}
		db.close();
	}
	
	public void deleteEntry(int entry_id) {
		SQLiteDatabase db = this.getReadableDatabase();
		db.delete(ENTRIES_TABLE_NAME, "_id=?", new String[] { Integer.toString(entry_id) });
		db.close();
	}

}
