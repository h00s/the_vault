package me.husak.thevault;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class EntryActivity extends Activity {
	
	private int entry_id = -1;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_entry);
		
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		Bundle extras = getIntent().getExtras();
		entry_id = extras.getInt("id");
		if (entry_id == -1) {
			setTitle(this.getString(R.string.add_entry));
		}
		else {
			EntriesOpenHelper entriesHelper = new EntriesOpenHelper(this.getApplicationContext());
			Entry entry = entriesHelper.getEntry(entry_id);
			entriesHelper.close();
			
			setTitle(entry.getTitle());
			
			EditText edTitle = (EditText) findViewById(R.id.etTitle);
			EditText edUsername = (EditText) findViewById(R.id.etUsername);
			EditText edPassword = (EditText) findViewById(R.id.etPassword);
			
			edTitle.setText(entry.getTitle());
			edUsername.setText(entry.getUsername());
			edPassword.setText(entry.getPassword());
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.entry, menu);
		if (entry_id == -1) {
			MenuItem item = menu.findItem(R.id.delete_entry);
			item.setVisible(false);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case android.R.id.home:
	            Intent intent = new Intent(this, EntriesActivity.class);
	            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	            startActivity(intent);
	            return true;
	        case R.id.delete_entry:
	        	EntriesOpenHelper entriesHelper = new EntriesOpenHelper(this.getApplicationContext());
	        	entriesHelper.deleteEntry(entry_id);
	        	entriesHelper.close();
	        	this.finish();
	        	return true;
	        case R.id.save_entry:
				EditText edTitle = (EditText) findViewById(R.id.etTitle);
				EditText edUsername = (EditText) findViewById(R.id.etUsername);
				EditText edPassword = (EditText) findViewById(R.id.etPassword);

				Entry entry = new Entry(entry_id, edTitle.getText().toString(), edUsername.getText().toString(), edPassword.getText().toString());
	    		EntriesOpenHelper entriesHelper2 = new EntriesOpenHelper(this.getApplicationContext());
	    		entriesHelper2.saveEntry(entry);
	    		entriesHelper2.close();
	    		this.finish();
	        	return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}
