package me.husak.thevault;

public class Entry {

	public Entry() {
		this.id = -1;
		this.title = "";
		this.username = "";
		this.password = "";
	}
	
	public Entry(int id, String title, String username, String password) {
		this.id = id;
		this.title = title;
		this.username = username;
		this.password = password;
	}
	
	public int getId() {
		return this.id;
	}
	
	public String getTitle() {
		return this.title;
	}

	public String getUsername() {
		return this.username;
	}

	public String getPassword() {
		return this.password;
	}

	private int id;
	private String title;
	private String username;
	private String password;

}
